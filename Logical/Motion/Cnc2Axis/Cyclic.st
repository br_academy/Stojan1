
PROGRAM _CYCLIC
	
	CASE State OF 
		0:
			// Wait
			
			
		1:
			// Power on
//			IF (axis1.Info.ReadyToPowerOn AND axis1.Info.ReadyToPowerOn) THEN
//				gCnc2Axis.Power := TRUE;
//				State := 2;
//			END_IF
			
		2:
			IF (gCnc2Axis.PowerOn) THEN
				gCnc2Axis.Home := TRUE;
				State := 3;
			ELSIF (gCnc2Axis.Error) THEN
				gCnc2Axis.Power := FALSE;
				State := 111;
			END_IF
			
		3:
			IF (gCnc2Axis.IsHomed) THEN
				gCnc2Axis.Home := FALSE;
				State := 4;
			ELSIF (gCnc2Axis.Error) THEN
				gCnc2Axis.Home := FALSE;
				gCnc2Axis.Power := FALSE;
				State := 111;
			END_IF
			
		4:
			// Wait
			
		5:
			// Start a program
			gCnc2AxisPar.ProgramName := 'Heart';
			gCnc2Axis.Override := 100;
			gCnc2Axis.MoveProgram := TRUE;
			State := 6;
			
		6:
			IF (gCnc2Axis.MoveDone) THEN
				gCnc2Axis.MoveProgram := FALSE;
				State := 7;
			ELSIF (gCnc2Axis.Error) THEN
				gCnc2Axis.MoveProgram := FALSE;
				gCnc2Axis.Power := FALSE;
				State := 111;
			END_IF
			
		7:
			// Program finished
				
		111:
			// Error
		
	END_CASE
	
	gCnc2Axis();
	 
END_PROGRAM
